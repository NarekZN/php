
$.fn.myMasonry = function(options) {
    var options = $.extend({ width: 300 }, options);
    this.each(function(){
    masonry($(this), options);
    window.addEventListener('resize', () => {
        setTimeout(() => {
            masonry($(this), options)
        }, 1500);
    })
    })
}

function masonry(container, options) { 
    var fullWidth = container.width();
    var li = container.find('.item');
    var imgColumns = [];
    var col = Math.floor(fullWidth / options.width);

    li.css({ width: options.width + "px"});    
    li.css({ top: 0, left: 0 });
    
    for (var i = 0; i < li.length; i++) {
        var minHeight = imgColumns[0];
        var indexColumn=0;
        var maxHeight = imgColumns[0];
        var size =Math.ceil(li.height() - container.find("img").height());
        if(imgColumns[i] === undefined) {
            imgColumns[i] = 0;
        }
        
        for (j = 0; j < col; j++) {
            if (minHeight > imgColumns[j]) {
                minHeight = imgColumns[j];
                indexColumn = j;
            }
        }
        
        li[i].style.left = indexColumn * options.width + 'px'; 
        li[i].style.top = minHeight + 'px';
        imgColumns[indexColumn] = imgColumns[indexColumn] + li[i].clientHeight-size ;
        if (maxHeight <  imgColumns[indexColumn]) {
            maxHeight =  imgColumns[indexColumn];   
        }  
        container.height(maxHeight +'px')
    }

}