<?php
 require_once 'admin/connect.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Masonry</title>

    <link rel="stylesheet" href="style.css">

    <script src="jquery.js"></script>
    <script src="masonry.js"></script>
    <script src="script-masonry.js"></script>
</head> 
<body>
    <ul class="masonry masonry1">

        <?php if ($result = $conn->query("SELECT * FROM images")) {
            while($row = mysqli_fetch_array($result)){ ?>
                <li class="item"><img class="img" src="../images/<?php echo $row['path'] ?>" alt="#" style="border:2px solid <?php echo $row['color'] ?>"></style></li>

            <?php  }?> 

        <?php  }?>         
        
    </ul>
    
</body>
</html>